﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ModulFinal
{
    public partial class Form1 : Form
    {
        Bitmap objBitmapInput;
        Bitmap objBitmapOutput1;
        Bitmap objBitmapOutput2;
        Bitmap objBitmapOutput3;
        Bitmap objBitmapOutput4;
        Bitmap objBitmapSwap;
        String mode = "RGB";

        public Form1()
        {
            InitializeComponent();
            disableButtonAwal();
        }

        private void toolStripLabel1_Click(object sender, EventArgs e)
        {

        }

        private void toolStripDropDownButton3_Click(object sender, EventArgs e)
        {

        }

        private void ToolStripButton5_Click(object sender, EventArgs e)
        {
            flushImageObject();
            flushPictureBox();

            DialogResult d = openFileDialog1.ShowDialog();
            if(d == DialogResult.OK)
            {
                objBitmapInput = new Bitmap(openFileDialog1.FileName);
                pictureBoxInput.Image = objBitmapInput;
            }
            if (objBitmapInput != null)
            {
                cetakHistogram();
                enableButtonAwal();
            }            
        }

        private void cetakHistogram()
        {
            float[] h = new float[256];
            int i;
            for (i = 0; i < 256; i++) h[i] = 0;
            
            for(int x = 0;x<objBitmapInput.Width; x++)
            {
                for(int y = 0; y < objBitmapInput.Height; y++)
                {
                    Color w = objBitmapInput.GetPixel(x, y);
                    int xg = w.R;
                    h[xg] = h[xg] + 1;
                }
                for(i = 0; i< 256; i++)
                {
                    chartHistogram.Series["Series1"].Points.AddXY(i, h[i]);
                }
            }
        }

        private void btnFlipHorizontal_Click(object sender, EventArgs e)
        {
            flushImageObject();

            objBitmapOutput1 = new Bitmap(objBitmapInput);
            for (int x = 0; x < objBitmapInput.Width; x++)
            {
                for(int y = 0; y< objBitmapInput.Height; y++)
                {
                    Color w = objBitmapInput.GetPixel(x, y);
                    objBitmapOutput1.SetPixel(objBitmapInput.Width - 1 - x, y, w);
                }
            }
            pictureBoxOutput1.Image = objBitmapOutput1;
            labelOutput1.Text = "Flip Horizontal";
            btnSwap.Enabled = true;

            objBitmapSwap = objBitmapOutput1;
        }

        private void btnFlipVertikal_Click(object sender, EventArgs e)
        {
            flushImageObject();

            objBitmapOutput1 = new Bitmap(objBitmapInput);
            for (int x = 0; x < objBitmapInput.Width; x++)
            {
                for (int y = 0; y < objBitmapInput.Height; y++)
                {
                    Color w = objBitmapInput.GetPixel(x, y);
                    objBitmapOutput1.SetPixel(x,objBitmapInput.Height - 1 - y, w);
                }
            }
            pictureBoxOutput1.Image = objBitmapOutput1;
            labelOutput1.Text = "Flip Vertikal";
            btnSwap.Enabled = true;

            objBitmapSwap = objBitmapOutput1;
        }

        private void btnLayerRGB_Click(object sender, EventArgs e)
        {
            flushImageObject();
            flushPictureBox();
            objBitmapOutput2 = new Bitmap(objBitmapInput);
            objBitmapOutput3 = new Bitmap(objBitmapInput);
            objBitmapOutput4 = new Bitmap(objBitmapInput);

            for (int x = 0; x<objBitmapInput.Width; x++)
                for(int y =0; y<objBitmapOutput2.Height; y++)
                {
                    Color w = objBitmapInput.GetPixel(x, y);
                    int r = w.R;
                    Color wb = Color.FromArgb(r, 0, 0);
                    objBitmapOutput2.SetPixel(x, y, wb);
                }

            for (int x = 0; x < objBitmapInput.Width; x++)
                for (int y = 0; y < objBitmapOutput3.Height; y++)
                {
                    Color w = objBitmapInput.GetPixel(x, y);
                    int g = w.G;
                    Color wb = Color.FromArgb(0, g, 0);
                    objBitmapOutput3.SetPixel(x, y, wb);
                }

            for(int x = 0; x<objBitmapInput.Width; x++)
                for(int y = 0; y<objBitmapOutput4.Height; y++)
                {
                    Color w = objBitmapInput.GetPixel(x, y);
                    int b = w.B;
                    Color wb = Color.FromArgb(0, 0, b);
                    objBitmapOutput4.SetPixel(x, y, wb);
                }

            pictureBoxOutput2.Image = objBitmapOutput2;
            labelOutput2.Text = "Filtered Red";
            pictureBoxOutput3.Image = objBitmapOutput3;
            labelOutput3.Text = "Filtered Green";
            pictureBoxOutput4.Image = objBitmapOutput4;
            labelOutput4.Text = "FIltered Blue";
        }

        private void btnLayerGrayscale_Click(object sender, EventArgs e)
        {
            flushImageObject();
            flushPictureBox();
            objBitmapOutput2 = new Bitmap(objBitmapInput);
            objBitmapOutput3 = new Bitmap(objBitmapInput);
            objBitmapOutput4 = new Bitmap(objBitmapInput);

            for (int x = 0; x < objBitmapInput.Width; x++)
                for (int y = 0; y < objBitmapOutput2.Height; y++)
                {
                    Color w = objBitmapInput.GetPixel(x, y);
                    int r = w.R;
                    Color wb = Color.FromArgb(r, r, r);
                    objBitmapOutput2.SetPixel(x, y, wb);
                }

            for (int x = 0; x < objBitmapInput.Width; x++)
                for (int y = 0; y < objBitmapOutput3.Height; y++)
                {
                    Color w = objBitmapInput.GetPixel(x, y);
                    int g = w.G;
                    Color wb = Color.FromArgb(g, g, g);
                    objBitmapOutput3.SetPixel(x, y, wb);
                }

            for (int x = 0; x < objBitmapInput.Width; x++)
                for (int y = 0; y < objBitmapOutput4.Height; y++)
                {
                    Color w = objBitmapInput.GetPixel(x, y);
                    int b = w.B;
                    Color wb = Color.FromArgb(b, b, b);
                    objBitmapOutput4.SetPixel(x, y, wb);
                }

            pictureBoxOutput2.Image = objBitmapOutput2;
            labelOutput2.Text = "Grayscale Filtered Red";
            pictureBoxOutput3.Image = objBitmapOutput3;
            labelOutput3.Text = "Grayscale Filtered Green";
            pictureBoxOutput4.Image = objBitmapOutput4;
            labelOutput4.Text = "Grayscale FIltered Blue";
        }

        private void btnKuantisasiCitra8_Click(object sender, EventArgs e)
        {
            flushImageObject();
            flushPictureBox();
            objBitmapOutput1 = new Bitmap(objBitmapInput);
            for (int x = 0; x < objBitmapInput.Width; x++)
                for (int y = 0; y < objBitmapOutput1.Height; y++)
                {
                    Color w = objBitmapInput.GetPixel(x, y);
                    int r = w.R;
                    int g = w.G;
                    int b = w.B;
                    int xg = (int)((r + g + b) / 3);
                    int xk = 8 * (int)(xg / 8);
                    Color wb = Color.FromArgb(xk, xk, xk);
                    objBitmapOutput1.SetPixel(x, y, wb);
                }
            pictureBoxOutput1.Image = objBitmapOutput1;
            labelOutput1.Text = "Kuantisasi Citra 8";
            btnSwap.Enabled = true;
            objBitmapSwap = objBitmapOutput1;
        }
        
        private void flushPictureBox()
        {
            pictureBoxOutput1.Image = null;
            labelOutput1.Text = "";
            pictureBoxOutput2.Image = null;
            labelOutput2.Text = "";
            pictureBoxOutput3.Image = null;
            labelOutput3.Text = "";
            pictureBoxOutput4.Image = null;
            labelOutput4.Text = "";
        }

        private void btnKuantisasiCitra16_Click(object sender, EventArgs e)
        {
            flushImageObject();
            flushPictureBox();
            objBitmapOutput1 = new Bitmap(objBitmapInput);
            for (int x = 0; x < objBitmapInput.Width; x++)
                for (int y = 0; y < objBitmapOutput1.Height; y++)
                {
                    Color w = objBitmapInput.GetPixel(x, y);
                    int r = w.R;
                    int g = w.G;
                    int b = w.B;
                    int xg = (int)((r + g + b) / 3);
                    int xk = 16 * (int)(xg / 16);
                    Color wb = Color.FromArgb(xk, xk, xk);
                    objBitmapOutput1.SetPixel(x, y, wb);
                }
            pictureBoxOutput1.Image = objBitmapOutput1;
            labelOutput1.Text = "Kuantisasi Citra 16";
            btnSwap.Enabled = true;
            objBitmapSwap = objBitmapOutput1;
        }

        private void btnKuantisasiCitra32_Click(object sender, EventArgs e)
        {
            flushImageObject();
            flushPictureBox();
            objBitmapOutput1 = new Bitmap(objBitmapInput);
            for (int x = 0; x < objBitmapInput.Width; x++)
                for (int y = 0; y < objBitmapOutput1.Height; y++)
                {
                    Color w = objBitmapInput.GetPixel(x, y);
                    int r = w.R;
                    int g = w.G;
                    int b = w.B;
                    int xg = (int)((r + g + b) / 3);
                    int xk = 32 * (int)(xg / 32);
                    Color wb = Color.FromArgb(xk, xk, xk);
                    objBitmapOutput1.SetPixel(x, y, wb);
                }
            pictureBoxOutput1.Image = objBitmapOutput1;
            labelOutput1.Text = "Kuantisasi Citra 32";
            btnSwap.Enabled = true;
            objBitmapSwap = objBitmapOutput1;
        }

        private void btnKuantisasiCitra64_Click(object sender, EventArgs e)
        {
            flushImageObject();
            flushPictureBox();
            objBitmapOutput1 = new Bitmap(objBitmapInput);
            for (int x = 0; x < objBitmapInput.Width; x++)
                for (int y = 0; y < objBitmapOutput1.Height; y++)
                {
                    Color w = objBitmapInput.GetPixel(x, y);
                    int r = w.R;
                    int g = w.G;
                    int b = w.B;
                    int xg = (int)((r + g + b) / 3);
                    int xk = 64 * (int)(xg / 64);
                    Color wb = Color.FromArgb(xk, xk, xk);
                    objBitmapOutput1.SetPixel(x, y, wb);
                }
            pictureBoxOutput1.Image = objBitmapOutput1;
            labelOutput1.Text = "Kuantisasi Citra 64";
            btnSwap.Enabled = true;
            objBitmapSwap = objBitmapOutput1;
        }

        private void btnKuantisasiCitra128_Click(object sender, EventArgs e)
        {
            flushImageObject();
            flushPictureBox();
            objBitmapOutput1 = new Bitmap(objBitmapInput);
            for (int x = 0; x < objBitmapInput.Width; x++)
                for (int y = 0; y < objBitmapOutput1.Height; y++)
                {
                    Color w = objBitmapInput.GetPixel(x, y);
                    int r = w.R;
                    int g = w.G;
                    int b = w.B;
                    int xg = (int)((r + g + b) / 3);
                    int xk = 128 * (int)(xg / 128);
                    Color wb = Color.FromArgb(xk, xk, xk);
                    objBitmapOutput1.SetPixel(x, y, wb);
                }
            pictureBoxOutput1.Image = objBitmapOutput1;
            labelOutput1.Text = "Kuantisasi Citra 128";
            btnSwap.Enabled = true;
            objBitmapSwap = objBitmapOutput1;
        }

        private void disableButtonAwal()
        {
            btnSave.Enabled = false;
            toolStripModeWarna.Enabled = false;
            toolStripModifikasiPosisi.Enabled = false;
            btnZoomIn.Enabled = false;
            btnZoomOut.Enabled = false;
            textBoxBrightness.Enabled = false;
            btnBrightness.Enabled = false;
            textBoxContrast.Enabled = false;
            btnContrast.Enabled = false;
            toolStripBangkitkanNoise.Enabled = false;
            toolStripReduksiNoise.Enabled = false;
            toolstripEdgeDetection.Enabled = false;
            toolStripKuantisasiCitra.Enabled = false;
            toolStripToolsLainnya.Enabled = false;
            btnSwap.Enabled = false;

            labelOutput1.Text = "";
            labelOutput2.Text = "";
            labelOutput3.Text = "";
            labelOutput4.Text = "";
        }

        private void enableButtonAwal()
        {
            btnSave.Enabled = true;
            toolStripModeWarna.Enabled = true;
            toolStripModifikasiPosisi.Enabled = true;
            btnZoomIn.Enabled = true;
            btnZoomOut.Enabled = true;
            textBoxBrightness.Enabled = true;
            btnBrightness.Enabled = true;
            textBoxContrast.Enabled = true;
            btnContrast.Enabled = true;
            toolStripBangkitkanNoise.Enabled = true;
            toolStripReduksiNoise.Enabled = true;
            toolstripEdgeDetection.Enabled = true;
            toolStripKuantisasiCitra.Enabled = true;
            toolStripToolsLainnya.Enabled = true;
        }

        private void btnModeGrayscale_Click(object sender, EventArgs e)
        {
            flushImageObject();
            mode = "GRAYSCALE";
            objBitmapOutput1 = new Bitmap(objBitmapInput);
            for(int x = 0; x < objBitmapInput.Width; x++)
                for(int y = 0; y < objBitmapOutput1.Height; y++)
                {
                    Color w = objBitmapInput.GetPixel(x, y);
                    int r = w.R;
                    int g = w.G;
                    int b = w.B;
                    int xg = (int)((r + g + b) / 3);
                    Color wb = Color.FromArgb(xg, xg, xg);
                    objBitmapOutput1.SetPixel(x, y, wb);
                }
            pictureBoxOutput1.Image = objBitmapOutput1;
            btnSwap.Enabled = true;
            labelOutput1.Text = "Grayscale";
            objBitmapSwap = objBitmapOutput1;
        }

        private void btnModeBW_Click(object sender, EventArgs e)
        {
            flushImageObject();
            objBitmapOutput1 = new Bitmap(objBitmapInput);
        for (int x = 0; x < objBitmapInput.Width; x++)
            for(int y = 0; y<objBitmapOutput1.Height; y++)
            {
                Color w = objBitmapInput.GetPixel(x, y);
                int r = w.R;
                int g = w.G;
                int b = w.B;
                int xg = (int)((r + g + b) / 3);
                int xbw = 0;
                if (xg >= 128) xbw = 255;
                Color wb = Color.FromArgb(xbw, xbw, xbw);
                objBitmapOutput1.SetPixel(x, y, wb);
            }
            pictureBoxOutput1.Image = objBitmapOutput1;
            btnSwap.Enabled = true;
            labelOutput1.Text = "Black & White";
            objBitmapSwap = objBitmapOutput1;
        }

        private void flushImageObject()
        {
            objBitmapOutput1 = null;
            objBitmapOutput2 = null;
            objBitmapOutput3 = null;
            objBitmapOutput4 = null;
        }

        private void btnSwap_Click(object sender, EventArgs e)
        {
            flushImageObject();

            objBitmapInput = new Bitmap(objBitmapSwap);
            for(int x = 0; x < objBitmapSwap.Width; x++)
                for(int y = 0; y < objBitmapInput.Height; y++)
                {
                    Color w = objBitmapSwap.GetPixel(x, y);
                    int r = w.R;
                    int g = w.G;
                    int b = w.B;
                    Color wb = Color.FromArgb(r, g, b);
                    objBitmapInput.SetPixel(x, y, wb);
                }
            pictureBoxInput.Image = objBitmapInput;
            cetakHistogram();
        }

        private void btnBrightness_Click(object sender, EventArgs e)
        {
            flushImageObject();
            objBitmapOutput1 = new Bitmap(objBitmapInput);
            int a = Convert.ToInt16(textBoxBrightness.Text);

            for(int x = 0; x < objBitmapInput.Width; x++)
            {
                for(int y = 0; y < objBitmapInput.Height; y++)
                {
                    Color w = objBitmapInput.GetPixel(x, y);
                    int xr = w.R; int xrBaru = xr + a;
                    int xg = w.G; int xgBaru = xg + a;
                    int xb = w.B; int xbBaru = xg + a;

                    if (xrBaru < 0) xrBaru = 0;
                    if (xrBaru > 255) xrBaru = 255;

                    if (xgBaru < 0) xgBaru = 0;
                    if (xgBaru > 255) xgBaru = 255;

                    if (xbBaru < 0) xbBaru = 0;
                    if (xbBaru > 255) xbBaru = 255;

                    Color wb = Color.FromArgb(xrBaru, xgBaru, xbBaru);
                    objBitmapOutput1.SetPixel(x, y, wb);
                }
            }
            pictureBoxOutput1.Image = objBitmapOutput1;
            btnSwap.Enabled = true;
            labelOutput1.Text = "Brightness + "+textBoxBrightness.Text;
            objBitmapSwap = objBitmapOutput1;
        }

        private void btnContrast_Click(object sender, EventArgs e)
        {
            flushImageObject();
            objBitmapOutput1 = new Bitmap(objBitmapInput);
            float c = Convert.ToSingle(textBoxContrast.Text);

            if (c < -100) c = -100;
            if (c > 100) c = 100;

            c = (100.0f + c) / 100.0f;

            for(int x = 0; x < objBitmapInput.Width; x++)
            {
                for(int y = 0; y<objBitmapInput.Height; y++)
                {
                    Color w = objBitmapInput.GetPixel(x, y);

                    float xr = w.R/255.0f;
                    xr -= 0.5f;
                    xr *= c;
                    xr += 0.5f;
                    xr *= 255;
                    if (xr < 0) xr = 0;
                    if (xr > 255) xr = 255;

                    float xg = w.G/255.0f;
                    xg -= 0.5f;
                    xg *= c;
                    xg += 0.5f;
                    xg *= 255;
                    if (xg < 0) xg = 0;
                    if (xg > 255) xg = 255;

                    float xb = w.B/255.0f;
                    xb -= 0.5f;
                    xb *= c;
                    xb += 0.5f;
                    xb *= 255;
                    if (xb < 0) xb = 0;
                    if (xb > 255) xb = 255;

                    Color wb = Color.FromArgb((byte)xr, (byte)xg, (byte)xb);
                    objBitmapOutput1.SetPixel(x, y, wb);
                }
            }

            pictureBoxOutput1.Image = objBitmapOutput1;
            btnSwap.Enabled = true;
            labelOutput1.Text = "Contrast + " + textBoxContrast.Text;
            objBitmapSwap = objBitmapOutput1;
        }

        private void btnInvers_Click(object sender, EventArgs e)
        {
            flushImageObject();
            objBitmapOutput1 = new Bitmap(objBitmapInput);

            for(int x = 0; x < objBitmapInput.Width; x++)
            {
                for(int y = 0; y < objBitmapInput.Height; y++)
                {
                    Color w = objBitmapInput.GetPixel(x, y);
                    int xr = w.R; int xrInv = (int)(255 - xr);
                    int xg = w.G; int xgInv = (int)(255 - xg);
                    int xb = w.B; int xbInv = (int)(255 - xb);
                    Color wb = Color.FromArgb(xrInv, xgInv, xbInv);
                    objBitmapOutput1.SetPixel(x, y, wb);                    
                }
            }
            pictureBoxOutput1.Image = objBitmapOutput1;
            btnSwap.Enabled = true;
            labelOutput1.Text = "Invers + ";
            objBitmapSwap = objBitmapOutput1;
        }

        private void btnAutoLevel_Click(object sender, EventArgs e)
        {
            flushImageObject();

            objBitmapOutput1 = new Bitmap(objBitmapInput);
            int warnaMaxRed = 0;
            int warnaMinRed = 255;

            int warnaMaxGreen = 0;
            int warnaMinGreen = 255;

            int warnaMaxBlue = 0;
            int warnaMinBlue = 255;

            for ( int x = 0; x <objBitmapInput.Width; x++)
            {
                for (int y = 0; y< objBitmapInput.Height; y++)
                {
                    Color w = objBitmapInput.GetPixel(x, y);
                    int xr = w.R;
                    if (xr < warnaMinRed) warnaMinRed = xr;
                    if (xr > warnaMaxRed) warnaMaxRed = xr;

                    int xg = w.G;
                    if (xg < warnaMinGreen) warnaMinGreen = xg;
                    if (xg > warnaMaxGreen) warnaMaxGreen = xg;

                    int xb = w.B;
                    if (xb < warnaMinBlue) warnaMinBlue = xb;
                    if (xb > warnaMaxBlue) warnaMaxBlue = xb;
                }
            }

            for(int x = 0; x<objBitmapInput.Width; x++)
            {
                for(int y = 0; y< objBitmapInput.Height; y++)
                {
                    Color w = objBitmapInput.GetPixel(x, y);
                    int xr = w.R;
                    int xrAL = (int)(255 * (xr - warnaMinRed) / (warnaMaxRed - warnaMinRed));
                    
                    int xg = w.G;
                    int xgAL = (int)(255 * (xg - warnaMinGreen) / (warnaMaxGreen - warnaMinGreen));

                    int xb = w.B;
                    int xbAL = (int)(255 * (xb - warnaMinBlue) / (warnaMaxBlue - warnaMinBlue));

                    Color wb = Color.FromArgb(xrAL, xgAL, xbAL);
                    objBitmapOutput1.SetPixel(x, y, wb);
                }
            }

            pictureBoxOutput1.Image = objBitmapOutput1;
            btnSwap.Enabled = true;
            labelOutput1.Text = "Invers + ";
            objBitmapSwap = objBitmapOutput1;
        }

        private void btnCDF_Click(object sender, EventArgs e)
        {
            flushImageObject();

            float[] h = new float[256];
            float[] c = new float[256];

            int i;

            for (i = 0; i < 256; i++) h[i] = 0;
            for (int x = 0; x <objBitmapInput.Width; x++)
            {
                for(int y = 0; y < objBitmapInput.Height; y++)
                {
                    Color w = objBitmapInput.GetPixel(x, y);
                    int xr = w.R;
                    h[xr] = h[xr] + 1;
                }
            }

            c[0] = h[0];
            for (i = 1; i < 256; i++) c[i] = c[i - 1] + h[i];
            for(i = 0; i < 256; i++)
            {
                chartHistogram.Series[0].Points.AddXY(i, c[i]);
            }
        }

        private void btnBangkitkanNoiseGaussian_Click(object sender, EventArgs e)
        {
            flushImageObject();

            objBitmapOutput1 = new Bitmap(objBitmapInput);
            Random r = new Random();
            for (int x = 0; x < objBitmapInput.Width; x++)
            {
                for (int y = 0; y < objBitmapInput.Height; y++)
                {
                    Color w = objBitmapInput.GetPixel(x, y);
                    int xr = w.R; int xrNoise = xr;
                    int xg = w.G; int xgNoise = xg;
                    int xb = w.B; int xbNoise = xb;
                    int nr = r.Next(0, 100);
                    if(nr < 5)
                    {
                        int ns = r.Next(0, 256) - 128;
                        xrNoise = (int)(xrNoise + ns);
                        if (xrNoise < 0) xrNoise = -xrNoise;
                        if (xrNoise > 255) xrNoise = 255;

                        xgNoise = (int)(xgNoise + ns);
                        if (xgNoise < 0) xgNoise = -xgNoise;
                        if (xgNoise > 255) xgNoise = 255;

                        xbNoise = (int)(xbNoise + ns);
                        if (xbNoise < 0) xbNoise = -xbNoise;
                        if (xbNoise > 255) xbNoise = 255;
                    }
                    Color wb = Color.FromArgb(xrNoise, xgNoise, xbNoise);
                    objBitmapOutput1.SetPixel(x, y, wb);
                }
            }
            
            pictureBoxOutput1.Image = objBitmapOutput1;
            btnSwap.Enabled = true;
            labelOutput1.Text = "Noise Gaussian";
            objBitmapSwap = objBitmapOutput1;
        }

        private void btnBangkitkanSpeckleNoise_Click(object sender, EventArgs e)
        {
            flushImageObject();

            objBitmapOutput1 = new Bitmap(objBitmapInput);
            Random r = new Random();
            for (int x = 0; x < objBitmapInput.Width; x++)
            {
                for (int y = 0; y < objBitmapInput.Height; y++)
                {
                    Color w = objBitmapInput.GetPixel(x, y);
                    int xr = w.R; int xrNoise = xr;
                    int xg = w.G; int xgNoise = xg;
                    int xb = w.B; int xbNoise = xb;
                    int nr = r.Next(0, 100);
                    if (nr < 5)
                    {
                        xrNoise = 0;
                        xgNoise = 0;
                        xbNoise = 0;
                    }
                    Color wb = Color.FromArgb(xrNoise, xgNoise, xbNoise);
                    objBitmapOutput1.SetPixel(x, y, wb);
                }
            }

            pictureBoxOutput1.Image = objBitmapOutput1;
            btnSwap.Enabled = true;
            labelOutput1.Text = "Noise Speckle";
            objBitmapSwap = objBitmapOutput1;
        }

        private void btnBangkitkanSaltPepperNoise_Click(object sender, EventArgs e)
        {
            flushImageObject();

            objBitmapOutput1 = new Bitmap(objBitmapInput);
            Random r = new Random();
            for (int x = 0; x < objBitmapInput.Width; x++)
            {
                for (int y = 0; y < objBitmapInput.Height; y++)
                {
                    Color w = objBitmapInput.GetPixel(x, y);
                    int xr = w.R; int xrNoise = xr;
                    int xg = w.G; int xgNoise = xg;
                    int xb = w.B; int xbNoise = xb;
                    int nr = r.Next(0, 100);
                    if (nr < 5)
                    {
                        xrNoise = 255;
                        xgNoise = 255;
                        xbNoise = 255;
                    }
                    Color wb = Color.FromArgb(xrNoise, xgNoise, xbNoise);
                    objBitmapOutput1.SetPixel(x, y, wb);
                }
            }

            pictureBoxOutput1.Image = objBitmapOutput1;
            btnSwap.Enabled = true;
            labelOutput1.Text = "Noise Salt & Pepper";
            objBitmapSwap = objBitmapOutput1;
        }

        private void btnReduksiRataNoise_Click(object sender, EventArgs e)
        {
            flushImageObject();

            objBitmapOutput1 = new Bitmap(objBitmapInput);
            for(int x = 1; x < objBitmapInput.Width -1; x++)
            {
                for(int y = 1; y <objBitmapInput.Height -1; y++)
                {
                    Color w1 = objBitmapInput.GetPixel(x - 1, y - 1);
                    Color w2 = objBitmapInput.GetPixel(x - 1, y);
                    Color w3 = objBitmapInput.GetPixel(x - 1, y + 1);
                    Color w4 = objBitmapInput.GetPixel(x, y - 1);
                    Color w5 = objBitmapInput.GetPixel(x, y);
                    Color w6 = objBitmapInput.GetPixel(x, y + 1);
                    Color w7 = objBitmapInput.GetPixel(x + 1, y - 1);
                    Color w8 = objBitmapInput.GetPixel(x + 1, y);
                    Color w9 = objBitmapInput.GetPixel(x + 1, y + 1);

                    int x1R = w1.R; int x1G = w1.G; int x1B = w1.B;
                    int x2R = w2.R; int x2G = w1.G; int x2B = w2.B;
                    int x3R = w3.R; int x3G = w1.G; int x3B = w3.B;
                    int x4R = w4.R; int x4G = w1.G; int x4B = w4.B;
                    int x5R = w5.R; int x5G = w1.G; int x5B = w5.B;
                    int x6R = w6.R; int x6G = w1.G; int x6B = w6.B;
                    int x7R = w7.R; int x7G = w1.G; int x7B = w7.B;
                    int x8R = w8.R; int x8G = w1.G; int x8B = w8.B;
                    int x9R = w9.R; int x9G = w1.G; int x9B = w9.B;

                    int xRed = (int)((x1R + x2R + x3R + x4R + x5R + x6R + x7R + x8R + x9R) / 9);
                    if (xRed < 0) xRed = 0;
                    if (xRed > 255) xRed = 255;

                    int xGreen = (int)((x1G + x2G + x3G + x4G + x5G + x6G + x7G + x8G + x9G) / 9);
                    if (xGreen < 0) xGreen = 0;
                    if (xGreen > 255) xGreen = 255;

                    int xBlue = (int)((x1B + x2B + x3B + x4B + x5B + x6B + x7B + x8B + x9B) / 9);
                    if (xBlue < 0) xBlue = 0;
                    if (xBlue > 255) xBlue = 255;

                    Color wb = Color.FromArgb(xRed, xGreen, xBlue);
                    objBitmapOutput1.SetPixel(x, y, wb);
                }
            }
            pictureBoxOutput1.Image = objBitmapOutput1;
            btnSwap.Enabled = true;
            labelOutput1.Text = "Filter Rata - Rata";
            objBitmapSwap = objBitmapOutput1;
        }

        private void btnReduksiGaussianNoise_Click(object sender, EventArgs e)
        {
            flushImageObject();

            objBitmapOutput1 = new Bitmap(objBitmapInput);
            for (int x = 1; x < objBitmapInput.Width - 1; x++)
            {
                for (int y = 1; y < objBitmapInput.Height - 1; y++)
                {
                    Color w1 = objBitmapInput.GetPixel(x - 1, y - 1);
                    Color w2 = objBitmapInput.GetPixel(x - 1, y);
                    Color w3 = objBitmapInput.GetPixel(x - 1, y + 1);
                    Color w4 = objBitmapInput.GetPixel(x, y - 1);
                    Color w5 = objBitmapInput.GetPixel(x, y);
                    Color w6 = objBitmapInput.GetPixel(x, y + 1);
                    Color w7 = objBitmapInput.GetPixel(x + 1, y - 1);
                    Color w8 = objBitmapInput.GetPixel(x + 1, y);
                    Color w9 = objBitmapInput.GetPixel(x + 1, y + 1);

                    int x1R = w1.R; int x1G = w1.G; int x1B = w1.B;
                    int x2R = w2.R; int x2G = w1.G; int x2B = w2.B;
                    int x3R = w3.R; int x3G = w1.G; int x3B = w3.B;
                    int x4R = w4.R; int x4G = w1.G; int x4B = w4.B;
                    int x5R = w5.R; int x5G = w1.G; int x5B = w5.B;
                    int x6R = w6.R; int x6G = w1.G; int x6B = w6.B;
                    int x7R = w7.R; int x7G = w1.G; int x7B = w7.B;
                    int x8R = w8.R; int x8G = w1.G; int x8B = w8.B;
                    int x9R = w9.R; int x9G = w1.G; int x9B = w9.B;

                    int xRed = (int)((x1R + x2R + x3R + x4R + 4 * x5R + x6R + x7R + x8R + x9R) / 13);
                    if (xRed < 0) xRed = 0;
                    if (xRed > 255) xRed = 255;

                    int xGreen = (int)((x1G + x2G + x3G + x4G + 4 * x5G + x6G + x7G + x8G + x9G) / 13);
                    if (xGreen < 0) xGreen = 0;
                    if (xGreen > 255) xGreen = 255;

                    int xBlue = (int)((x1B + x2B + x3B + x4B + 4 * x5B + x6B + x7B + x8B + x9B) / 13);
                    if (xBlue < 0) xBlue = 0;
                    if (xBlue > 255) xBlue = 255;

                    Color wb = Color.FromArgb(xRed, xGreen, xBlue);
                    objBitmapOutput1.SetPixel(x, y, wb);
                }
            }
            pictureBoxOutput1.Image = objBitmapOutput1;
            btnSwap.Enabled = true;
            labelOutput1.Text = "Filter Gaussian";
            objBitmapSwap = objBitmapOutput1;
        }

        private void btnReduksiMedianNoise_Click(object sender, EventArgs e)
        {
            flushImageObject();

            int[] xtR = new int[10];
            int[] xtG = new int[10];
            int[] xtB = new int[10];

            objBitmapOutput1 = new Bitmap(objBitmapInput);
            for (int x = 1; x < objBitmapInput.Width - 1; x++)
            {
                for (int y = 1; y < objBitmapInput.Height - 1; y++)
                {
                    Color w1 = objBitmapInput.GetPixel(x - 1, y - 1);
                    Color w2 = objBitmapInput.GetPixel(x - 1, y);
                    Color w3 = objBitmapInput.GetPixel(x - 1, y + 1);
                    Color w4 = objBitmapInput.GetPixel(x, y - 1);
                    Color w5 = objBitmapInput.GetPixel(x, y);
                    Color w6 = objBitmapInput.GetPixel(x, y + 1);
                    Color w7 = objBitmapInput.GetPixel(x + 1, y - 1);
                    Color w8 = objBitmapInput.GetPixel(x + 1, y);
                    Color w9 = objBitmapInput.GetPixel(x + 1, y + 1);

                    xtR[1] = w1.R; xtG[1] = w1.G; xtB[1] = w1.B;
                    xtR[2] = w2.R; xtG[2] = w1.G; xtB[2] = w2.B;
                    xtR[3] = w3.R; xtG[3] = w1.G; xtB[3] = w3.B;
                    xtR[4] = w4.R; xtG[4] = w1.G; xtB[4] = w4.B;
                    xtR[5] = w5.R; xtG[5] = w1.G; xtB[5] = w5.B;
                    xtR[6] = w6.R; xtG[6] = w1.G; xtB[6] = w6.B;
                    xtR[7] = w7.R; xtG[7] = w1.G; xtB[7] = w7.B;
                    xtR[8] = w8.R; xtG[8] = w1.G; xtB[8] = w8.B;
                    xtR[9] = w9.R; xtG[9] = w1.G; xtB[9] = w9.B;

                    for(int i = 1; i<9; i++)
                    {
                        for(int j = 1; j< 9; j++)
                        {
                            if(xtR[j] < xtR[j + 1])
                            {
                                int r = xtR[j];
                                xtR[j] = xtR[j + 1];
                                xtR[j + 1] = r;
                            }

                            if (xtG[j] < xtG[j + 1])
                            {
                                int g = xtG[j];
                                xtG[j] = xtG[j + 1];
                                xtG[j + 1] = g;
                            }

                            if (xtB[j] < xtB[j + 1])
                            {
                                int b = xtB[j];
                                xtB[j] = xtB[j + 1];
                                xtB[j + 1] = b;
                            }
                        }
                    }

                    int xr = xtR[5];
                    int xg = xtG[5];
                    int xb = xtB[5];

                    Color wb = Color.FromArgb(xr, xg, xb);
                    objBitmapOutput1.SetPixel(x, y, wb);
                }
            }
            pictureBoxOutput1.Image = objBitmapOutput1;
            btnSwap.Enabled = true;
            labelOutput1.Text = "Filter Median";
            objBitmapSwap = objBitmapOutput1;
        }

        private void btnEdgeRobert_Click(object sender, EventArgs e)
        {
            flushImageObject();
            objBitmapOutput1 = new Bitmap(objBitmapInput);

            for(int x = 1; x < objBitmapInput.Width; x++)
            {
                for(int y = 1; y < objBitmapInput.Height; y++)
                {
                    Color w1 = objBitmapInput.GetPixel(x - 1, y);
                    Color w2 = objBitmapInput.GetPixel(x, y);
                    Color w3 = objBitmapInput.GetPixel(x, y - 1);
                    Color w4 = objBitmapInput.GetPixel(x, y);

                    int x1R = w1.R; int x1G = w1.G; int x1B = w1.B;
                    int x2R = w2.R; int x2G = w2.G; int x2B = w2.B;
                    int x3R = w3.R; int x3G = w3.G; int x3B = w3.B;
                    int x4R = w4.R; int x4G = w4.G; int x4B = w4.B;

                    int xr = (int)((x2R - x1R) + (x4R - x3R));
                    if (xr < 0) xr = -xr;
                    if (xr > 255) xr = 255;

                    int xg = (int)((x2G - x1G) + (x4G - x3G));
                    if (xg < 0) xg = -xg;
                    if (xg > 255) xg = 255;

                    int xb = (int)((x2B - x1B) + (x4B - x3B));
                    if (xb < 0) xb = -xb;
                    if (xb > 255) xb = 255;

                    Color wb = Color.FromArgb(xr, xg, xb);
                    objBitmapOutput1.SetPixel(x, y, wb);
                }
            }
            pictureBoxOutput1.Image = objBitmapOutput1;
            btnSwap.Enabled = true;
            labelOutput1.Text = "Filter Median";
            objBitmapSwap = objBitmapOutput1;
        }

        private void btnEdgePrewitt_Click(object sender, EventArgs e)
        {
            flushImageObject();

            objBitmapOutput1 = new Bitmap(objBitmapInput);
            for (int x = 1; x < objBitmapInput.Width - 1; x++)
            {
                for (int y = 1; y < objBitmapInput.Height - 1; y++)
                {
                    Color w1 = objBitmapInput.GetPixel(x - 1, y - 1);
                    Color w2 = objBitmapInput.GetPixel(x - 1, y);
                    Color w3 = objBitmapInput.GetPixel(x - 1, y + 1);
                    Color w4 = objBitmapInput.GetPixel(x, y - 1);
                    Color w5 = objBitmapInput.GetPixel(x, y);
                    Color w6 = objBitmapInput.GetPixel(x, y + 1);
                    Color w7 = objBitmapInput.GetPixel(x + 1, y - 1);
                    Color w8 = objBitmapInput.GetPixel(x + 1, y);
                    Color w9 = objBitmapInput.GetPixel(x + 1, y + 1);

                    int x1R = w1.R; int x1G = w1.G; int x1B = w1.B;
                    int x2R = w2.R; int x2G = w1.G; int x2B = w2.B;
                    int x3R = w3.R; int x3G = w1.G; int x3B = w3.B;
                    int x4R = w4.R; int x4G = w1.G; int x4B = w4.B;
                    int x5R = w5.R; int x5G = w1.G; int x5B = w5.B;
                    int x6R = w6.R; int x6G = w1.G; int x6B = w6.B;
                    int x7R = w7.R; int x7G = w1.G; int x7B = w7.B;
                    int x8R = w8.R; int x8G = w1.G; int x8B = w8.B;
                    int x9R = w9.R; int x9G = w1.G; int x9B = w9.B;

                    int xhRed = (int)(-x1R - x4R - x7R + x3R + x6R + x9R);
                    int xvRed = (int)(-x1R - x2R - x3R + x7R + x8R + x9R);
                    int xRed = (int)(xhRed + xvRed);
                    if (xRed < 0) xRed = 0;
                    if (xRed > 255) xRed = 255;

                    int xhGreen = (int)(-x1G - x4G - x7G + x3G + x6G + x9G);
                    int xvGreen = (int)(-x1G - x2G - x3G + x7G + x8G + x9G);
                    int xGreen = (int)(xhGreen + xvGreen);
                    if (xGreen < 0) xGreen = 0;
                    if (xGreen > 255) xGreen = 255;

                    int xhBlue = (int)(-x1B - x4B - x7B + x3B + x6B + x9B);
                    int xvBlue = (int)(-x1B - x2B - x3B + x7B + x8B + x9B);
                    int xBlue = (int)(xhBlue + xvBlue);
                    if (xBlue < 0) xBlue = 0;
                    if (xBlue > 255) xBlue = 255;

                    Color wb = Color.FromArgb(xRed, xGreen, xBlue);
                    objBitmapOutput1.SetPixel(x, y, wb);
                }
            }
            pictureBoxOutput1.Image = objBitmapOutput1;
            btnSwap.Enabled = true;
            labelOutput1.Text = "Filter Prewitt";
            objBitmapSwap = objBitmapOutput1;
        }

        private void btnEdgeSobel_Click(object sender, EventArgs e)
        {
            flushImageObject();

            objBitmapOutput1 = new Bitmap(objBitmapInput);
            for (int x = 1; x < objBitmapInput.Width - 1; x++)
            {
                for (int y = 1; y < objBitmapInput.Height - 1; y++)
                {
                    Color w1 = objBitmapInput.GetPixel(x - 1, y - 1);
                    Color w2 = objBitmapInput.GetPixel(x - 1, y);
                    Color w3 = objBitmapInput.GetPixel(x - 1, y + 1);
                    Color w4 = objBitmapInput.GetPixel(x, y - 1);
                    Color w5 = objBitmapInput.GetPixel(x, y);
                    Color w6 = objBitmapInput.GetPixel(x, y + 1);
                    Color w7 = objBitmapInput.GetPixel(x + 1, y - 1);
                    Color w8 = objBitmapInput.GetPixel(x + 1, y);
                    Color w9 = objBitmapInput.GetPixel(x + 1, y + 1);

                    int x1R = w1.R; int x1G = w1.G; int x1B = w1.B;
                    int x2R = w2.R; int x2G = w1.G; int x2B = w2.B;
                    int x3R = w3.R; int x3G = w1.G; int x3B = w3.B;
                    int x4R = w4.R; int x4G = w1.G; int x4B = w4.B;
                    int x5R = w5.R; int x5G = w1.G; int x5B = w5.B;
                    int x6R = w6.R; int x6G = w1.G; int x6B = w6.B;
                    int x7R = w7.R; int x7G = w1.G; int x7B = w7.B;
                    int x8R = w8.R; int x8G = w1.G; int x8B = w8.B;
                    int x9R = w9.R; int x9G = w1.G; int x9B = w9.B;

                    int xhRed = (int)(-x1R - 2 * x4R - x7R + x3R + 2 * x6R + x9R);
                    int xvRed = (int)(-x1R - 2 * x2R - x3R + x7R + 2 * x8R + x9R);
                    int xRed = (int)(xhRed + xvRed);
                    if (xRed < 0) xRed = 0;
                    if (xRed > 255) xRed = 255;

                    int xhGreen = (int)(-x1G - 2 * x4G - x7G + x3G + 2 * x6G + x9G);
                    int xvGreen = (int)(-x1G - 2 * x2G - x3G + x7G + 2 * x8G + x9G);
                    int xGreen = (int)(xhGreen + xvGreen);
                    if (xGreen < 0) xGreen = 0;
                    if (xGreen > 255) xGreen = 255;

                    int xhBlue = (int)(-x1B - 2 * x4B - x7B + x3B + 2 * x6B + x9B);
                    int xvBlue = (int)(-x1B - 2 * x2B - x3B + x7B + 2 * x8B + x9B);
                    int xBlue = (int)(xhBlue + xvBlue);
                    if (xBlue < 0) xBlue = 0;
                    if (xBlue > 255) xBlue = 255;

                    Color wb = Color.FromArgb(xRed, xGreen, xBlue);
                    objBitmapOutput1.SetPixel(x, y, wb);
                }
            }
            pictureBoxOutput1.Image = objBitmapOutput1;
            btnSwap.Enabled = true;
            labelOutput1.Text = "Filter Sobel";
            objBitmapSwap = objBitmapOutput1;
        }

        private void btnEdgeLaplacian_Click(object sender, EventArgs e)
        {
            flushImageObject();

            objBitmapOutput1 = new Bitmap(objBitmapInput);
            for (int x = 1; x < objBitmapInput.Width - 1; x++)
            {
                for (int y = 1; y < objBitmapInput.Height - 1; y++)
                {
                    Color w1 = objBitmapInput.GetPixel(x - 1, y - 1);
                    Color w2 = objBitmapInput.GetPixel(x - 1, y);
                    Color w3 = objBitmapInput.GetPixel(x - 1, y + 1);
                    Color w4 = objBitmapInput.GetPixel(x, y - 1);
                    Color w5 = objBitmapInput.GetPixel(x, y);
                    Color w6 = objBitmapInput.GetPixel(x, y + 1);
                    Color w7 = objBitmapInput.GetPixel(x + 1, y - 1);
                    Color w8 = objBitmapInput.GetPixel(x + 1, y);
                    Color w9 = objBitmapInput.GetPixel(x + 1, y + 1);

                    int x1R = w1.R; int x1G = w1.G; int x1B = w1.B;
                    int x2R = w2.R; int x2G = w1.G; int x2B = w2.B;
                    int x3R = w3.R; int x3G = w1.G; int x3B = w3.B;
                    int x4R = w4.R; int x4G = w1.G; int x4B = w4.B;
                    int x5R = w5.R; int x5G = w1.G; int x5B = w5.B;
                    int x6R = w6.R; int x6G = w1.G; int x6B = w6.B;
                    int x7R = w7.R; int x7G = w1.G; int x7B = w7.B;
                    int x8R = w8.R; int x8G = w1.G; int x8B = w8.B;
                    int x9R = w9.R; int x9G = w1.G; int x9B = w9.B;

                    int xRed = (int)(x1R - 2 * x2R + x3R - 2 * x4R + 4 * x5R - 2 * x6R + x7R);
                    if (xRed < 0) xRed = 0;
                    if (xRed > 255) xRed = 255;

                    int xGreen = (int)(x1G - 2 * x2G + x3G - 2 * x4G + 4 * x5G - 2 * x6G + x7G);
                    if (xGreen < 0) xGreen = 0;
                    if (xGreen > 255) xGreen = 255;

                    int xBlue = (int)(x1R - 2 * x2B + x3B - 2 * x4B + 4 * x5B - 2 * x6B + x7B);
                    if (xBlue < 0) xBlue = 0;
                    if (xBlue > 255) xBlue = 255;

                    Color wb = Color.FromArgb(xRed, xGreen, xBlue);
                    objBitmapOutput1.SetPixel(x, y, wb);
                }
            }
            pictureBoxOutput1.Image = objBitmapOutput1;
            btnSwap.Enabled = true;
            labelOutput1.Text = "Filter Laplacian";
            objBitmapSwap = objBitmapOutput1;
        }

        private void btnSharpening_Click(object sender, EventArgs e)
        {
            flushImageObject();

            objBitmapOutput1 = new Bitmap(objBitmapInput);
            for (int x = 1; x < objBitmapInput.Width - 1; x++)
            {
                for (int y = 1; y < objBitmapInput.Height - 1; y++)
                {
                    Color w1 = objBitmapInput.GetPixel(x - 1, y - 1);
                    Color w2 = objBitmapInput.GetPixel(x - 1, y);
                    Color w3 = objBitmapInput.GetPixel(x - 1, y + 1);
                    Color w4 = objBitmapInput.GetPixel(x, y - 1);
                    Color w5 = objBitmapInput.GetPixel(x, y);
                    Color w6 = objBitmapInput.GetPixel(x, y + 1);
                    Color w7 = objBitmapInput.GetPixel(x + 1, y - 1);
                    Color w8 = objBitmapInput.GetPixel(x + 1, y);
                    Color w9 = objBitmapInput.GetPixel(x + 1, y + 1);

                    int x1R = w1.R; int x1G = w1.G; int x1B = w1.B;
                    int x2R = w2.R; int x2G = w1.G; int x2B = w2.B;
                    int x3R = w3.R; int x3G = w1.G; int x3B = w3.B;
                    int x4R = w4.R; int x4G = w1.G; int x4B = w4.B;
                    int x5R = w5.R; int x5G = w1.G; int x5B = w5.B;
                    int x6R = w6.R; int x6G = w1.G; int x6B = w6.B;
                    int x7R = w7.R; int x7G = w1.G; int x7B = w7.B;
                    int x8R = w8.R; int x8G = w1.G; int x8B = w8.B;
                    int x9R = w9.R; int x9G = w1.G; int x9B = w9.B;

                    int xt1R = (int)((x1R + x2R + x3R + x4R + x5R + x6R + x7R + x8R + x9R) / 9);
                    int xt2R = (int)(-x1R - 2 * x2R - x3R + x7R + 2 * x8R + x9R);
                    int xt3R = (int)(-x1R - 2 * x4R - x7R + x3R + 2 * x6R + x9R);
                    int xRed = (int)(xt1R + xt2R + xt3R);
                    if (xRed < 0) xRed = -xRed;
                    if (xRed > 255) xRed = 255;

                    int xt1G = (int)((x1G + x2G + x3G + x4G + x5G + x6G + x7G + x8G + x9G) / 9);
                    int xt2G = (int)(-x1G - 2 * x2G - x3G + x7G + 2 * x8G + x9G);
                    int xt3G = (int)(-x1G - 2 * x4G - x7G + x3G + 2 * x6G + x9G);
                    int xGreen = (int)(xt1G + xt2G + xt3G);
                    if (xGreen < 0) xGreen = -xGreen;
                    if (xGreen > 255) xGreen = 255;

                    int xt1B = (int)((x1B + x2B + x3B + x4B + x5B + x6B + x7B + x8B + x9B) / 9);
                    int xt2B = (int)(-x1B - 2 * x2B - x3B + x7B + 2 * x8B + x9B);
                    int xt3B = (int)(-x1B - 2 * x4B - x7B + x3B + 2 * x6B + x9B);
                    int xBlue = (int)(xt1B + xt2B + xt3B);
                    if (xBlue < 0) xBlue = -xBlue;
                    if (xBlue > 255) xBlue = 255;

                    Color wb = Color.FromArgb(xRed, xGreen, xBlue);
                    objBitmapOutput1.SetPixel(x, y, wb);
                }
            }
            pictureBoxOutput1.Image = objBitmapOutput1;
            btnSwap.Enabled = true;
            labelOutput1.Text = "Sharpen";
            objBitmapSwap = objBitmapOutput1;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Filter = "JPG images (*.jpg)|*.jpg";
            saveFileDialog1.Title = "Save an Image File"; 
            DialogResult saveDialog = saveFileDialog1.ShowDialog();
            if (saveDialog == DialogResult.OK)
            {
                objBitmapInput.Save(saveFileDialog1.FileName, System.Drawing.Imaging.ImageFormat.Jpeg);
            }
        }
    }
}
